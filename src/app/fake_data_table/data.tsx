import {
    CheckCircledIcon,
    CircleIcon,
    CrossCircledIcon,
    StopwatchIcon,
  } from "@radix-ui/react-icons"
  
  export const statuses = [
    {
      value: "todo",
      label: "آرشیو شده",
      icon: CircleIcon,
    },
    {
      value: "in progress",
      label: "زمان‌بندی شده",
      icon: StopwatchIcon,
    },
    {
      value: "done",
      label: "منتشر شده",
      icon: CheckCircledIcon,
    },
    {
      value: "canceled",
      label: "عدم انتشار",
      icon: CrossCircledIcon,
    },
  ]