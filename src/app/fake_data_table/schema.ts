import { z } from "zod"

export const productSchema = z.object({
  id: z.string(),
  thumb: z.string(),
  productName: z.string(),
  stock: z.string(),
  unitPrice: z.number(),
  commentCount: z.number(),
  order: z.string(),
  special: z.boolean(),
  release: z.string(),
  operator: z.object({
    operatorName: z.string(),
    phoneNumber: z.string(),
  }),
  status: z.string(),
})


export type Product = z.infer<typeof productSchema>