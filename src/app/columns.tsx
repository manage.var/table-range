"use client"
import Image from "next/image"
import { ColumnDef } from "@tanstack/react-table"
import { Badge } from "@/components/ui/badge"
import { Checkbox } from "@/components/ui/checkbox"
import { statuses } from "./fake_data_table/data"
import { Product } from "./fake_data_table/schema"
import { DataTableColumnHeader } from "@/components/table/data-table-column-header"
import { DataTableRowActions } from "@/components/table/data-table-row-actions"


export const columns: ColumnDef<Product>[] = [
  {
    id: "select",
    header: ({ table }) => (
      <Checkbox
        checked={
          table.getIsAllPageRowsSelected() ||
          (table.getIsSomePageRowsSelected() && "indeterminate")
        }
        onCheckedChange={(value) => table.toggleAllPageRowsSelected(!!value)}
        aria-label="Select all"
        className="translate-y-[2px]"
      />
    ),
    cell: ({ row }) => (
      <Checkbox
        checked={row.getIsSelected()}
        onCheckedChange={(value) => row.toggleSelected(!!value)}
        aria-label="Select row"
        className="translate-y-[2px]"
      />
    ),
    enableSorting: false,
    enableHiding: false,
  },
  {
    accessorKey: "id",
    header: ({ column }) => (
      <DataTableColumnHeader column={column} title="شناسه" />
    ),
    cell: ({ row }) => <div className="w-[80px]">{row.getValue("id")}</div>,
    enableSorting: false,
    enableHiding: false,
  },
  {
    accessorKey: "thumb",
    header: "تصویر",
    cell: ({ row }) => {

      const thumbFilename = row.getValue("thumb");
      const thumbUrl = thumbFilename ? `/${thumbFilename}` : "/public/default-thumb.png";

      return (
        <div>
          <Image
            alt="Product image"
            className="aspect-square rounded-md object-cover"
            height="64"
            src={thumbUrl}
            width="64"
          />
        </div>
      )
    },
  },
  {
    accessorKey: "productName",
    header: ({ column }) => (
      <DataTableColumnHeader column={column} title="نام کالا" />
    ),
    cell: ({ row }) => {

      return (
        <div className="flex space-x-2 space-x-reverse">
          <span className="max-w-[500px] truncate font-medium">
            {row.getValue("productName")}
          </span>
        </div>
      )
    },
  },
  {
    accessorKey: "status",
    header: ({ column }) => (
      <DataTableColumnHeader column={column} title="وضعیت" />
    ),
    cell: ({ row }) => {
      const status = statuses.find(
        (status) => status.value === row.getValue("status")
      )

      if (!status) {
        return null
      }

      return (
        <div className="flex w-[100px] items-center">
          {status.icon && (
            <status.icon className="ml-2 h-4 w-4 text-muted-foreground" />
          )}
          <span>{status.label}</span>
        </div>
      )
    },
    filterFn: (row, id, value) => {
      return value.includes(row.getValue(id))
    },
  },
  {
    accessorKey: "unitPrice",
    header: ({ column }) => (
      <DataTableColumnHeader column={column} title="قیمت واحد" />
    ),
    cell: ({ row }) => {
      const unitPrice = parseFloat(row.getValue("unitPrice"))
      const formatted = new Intl.NumberFormat("fa-IR", {
        style: "decimal",
        maximumFractionDigits: 0,
      }).format(unitPrice)

      return (
        <div className="text-right font-medium">
          {formatted} <span>تومان</span>
        </div>
      )
    },
  },
  {
    accessorKey: "operator",
    header: ({ column }) => (
      <DataTableColumnHeader column={column} title="نام اپراتور" />
    ),
    cell: ({ row }) => {
      const operator = row.getValue("operator") as {
        operatorName: string;
        phoneNumber: string;
      };
      return (
        <div className="capitalize">
          {operator.operatorName}
          <p>{operator.phoneNumber}</p>
        </div>
      );
    },
  },
  {
    id: "actions",
    cell: ({ row }) => <DataTableRowActions row={row} />,
  },
]