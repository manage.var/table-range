import { promises as fs } from "fs"
import path from "path"
import { Metadata } from "next"
import Image from "next/image"
import { z } from "zod"

import { columns } from "./columns"
import { DataTable } from "@/components/table/data-table"
// import { UserNav } from "./components/user-nav"
import { productSchema } from "./fake_data_table/schema"

export const metadata: Metadata = {
  title: "Products List",
  description: "A task and issue tracker build using Tanstack Table.",
}

// Simulate a database read for products.
async function getProducts() {
  const data = await fs.readFile(
    path.join(process.cwd(), "src/app/fake_data_table/tasks.json")
  )

  const products = JSON.parse(data.toString())

  return z.array(productSchema).parse(products)
}

export default async function TaskPage() {
  const products = await getProducts()

  return (
    <>
      <div className="hidden h-full flex-1 flex-col space-y-8 p-8 md:flex">
        <div className="flex items-center justify-between space-y-2">
          <div>
            <h2 className="text-2xl font-bold tracking-tight">Welcome back!</h2>
            <p className="text-muted-foreground">
              Here&apos;s a list of your products for this month!
            </p>
          </div>
          {/* <div className="flex items-center space-x-2">
            <UserNav />
          </div> */}
        </div>
        <DataTable data={products} columns={columns} />
      </div>
    </>
  )
}