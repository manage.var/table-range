import { Search } from "lucide-react";
import { Table } from "@tanstack/react-table";
import { Input } from "@/components/ui/input";

interface DataTableSearchInputProps<TData> {
  table: Table<TData>;
}

export function DataTableSearch<TData>({ table }: DataTableSearchInputProps<TData>) {
  return (
    <div className="relative">
      <Search className="absolute right-3 top-4 h-4 w-4 opacity-50" />
      <Input
        placeholder="جستجوی کالا ..."
        value={(table.getColumn("productName")?.getFilterValue() as string) ?? ""}
        onChange={(event) =>
          table.getColumn("productName")?.setFilterValue(event.target.value)
        }
        className="h-12 w-[150px] lg:w-[650px] pr-9"
      />
    </div>
  );
}