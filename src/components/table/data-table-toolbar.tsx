"use client"

import { Cross2Icon } from "@radix-ui/react-icons"
import { Table } from "@tanstack/react-table"
import { Button } from "@/components/ui/button"
import { statuses } from "@/app/fake_data_table/data"
import { DataTableFacetedFilter } from "./data-table-faceted-filter"
import { DataTableRangeFilter } from "./data-table-range-filter"
import { DataTableSearch } from "./data-table-search";

interface DataTableToolbarProps<TData> {
  table: Table<TData>
}

export function DataTableToolbar<TData>({
  table,
}: DataTableToolbarProps<TData>) {
  const isFiltered = table.getState().columnFilters.length > 0

  return (
    <div className="flex items-center justify-between">
      <div className="flex flex-1 items-center space-x-2 space-x-reverse">
        <DataTableSearch table={table} />
        {table.getColumn("status") && (
          <DataTableFacetedFilter
            column={table.getColumn("status")}
            title="وضعیت"
            options={statuses}
          />
        )}
        {
          <DataTableRangeFilter
            column={table.getColumn("unitPrice")}
            table={table}
            title="محدوده قیمت"
          />
        }
        {isFiltered && (
          <Button
            variant="ghost"
            onClick={() => table.resetColumnFilters()}
            className="h-8 px-2 lg:px-3"
          >
            Reset
            <Cross2Icon className="ml-2 h-4 w-4" />
          </Button>
        )}
      </div>
    </div>
  )
}