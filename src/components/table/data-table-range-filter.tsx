import * as React from "react"
import { Column } from "@tanstack/react-table"
import { Slider } from "@/components/ui/slider"
import { Table } from "@tanstack/react-table"
import { useState, useMemo } from "react"



interface DataTableRangeFilterProps<TData, TValue> {
  column?: Column<TData, TValue>
  table: Table<TData>;
  title: string;
}


export function DataTableRangeFilter<TData, TValue>({
  column,
  table,
  title,
}: DataTableRangeFilterProps<TData, TValue>) {

  const filteredRows = table.getFilteredRowModel().flatRows
  const [filterRange, setFilterRange] = useState({
     range: [0, 0] 
  })

  const [minNum, maxNum] = useMemo(() => {
    
    const numValues = filteredRows.map((row) => row.original[column.id])
    return [Math.min(...numValues), Math.max(...numValues)];
  }, [filteredRows, column]);
  
  const MINIMUM = minNum;
  const MAXIMUM = maxNum;

  const handleFilterChange = (range: [number, number]) => {
    setFilterRange({ range });
    
    if (column?.setFilterValue) {
      // Assuming the column data type is number for range filter
      column.setFilterValue((row) => row.values[column.id] >= range[0] && row.values[column.id] <= range[1]);
      // Trigger table to re-render with filter applied
      table.setState({ ...table.getState(), filters: table.getState().filters }); 
    }
    if (column?.values) { // Check if column.values is defined
      column.setFilterValue((row) => row.values[column.id] >= range[0] && row.values[column.id] <= range[1]);
    }
  
  };


  return (
    <div className="w-40">
      <div className='flex justify-between'>
        <p className='font-medium'>{title}</p>
        <div>
            {MINIMUM.toFixed(0)} - {MAXIMUM.toFixed(0)}
          </div>
      </div>
      <Slider
        onValueChange={handleFilterChange}
        value={filterRange.range}
        min={MINIMUM}
        max={MAXIMUM}
        step={500}
      />
    </div>
  );
}